/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  19/03/2021 11:39:28                      */
/*==============================================================*/


drop table if exists BELONG;

drop table if exists CATEGORY;

drop table if exists COMMENTS;

drop table if exists LEND;

drop table if exists MANAGE;

drop table if exists MOVIE;

drop table if exists USERS;

/*==============================================================*/
/* Table : BELONG                                               */
/*==============================================================*/
create table BELONG
(
   ID_CATEGORY          char(10) not null,
   ID_MOVIE             bigint not null,
   primary key (ID_CATEGORY, ID_MOVIE)
);

/*==============================================================*/
/* Table : CATEGORY                                             */
/*==============================================================*/
create table CATEGORY
(
   ID_CATEGORY          char(10) not null,
   NAME                 varchar(255),
   primary key (ID_CATEGORY)
);

/*==============================================================*/
/* Table : COMMENTS                                             */
/*==============================================================*/
create table COMMENTS
(
   ID_USER              bigint not null,
   ID_MOVIE             bigint not null,
   ID_COMMENT           bigint,
   CONTENT              varchar(1024),
   primary key (ID_USER, ID_MOVIE)
);

/*==============================================================*/
/* Table : LEND                                                 */
/*==============================================================*/
create table LEND
(
   ID_USER              bigint not null,
   ID_MOVIE             bigint not null,
   CONTENT              varchar(1024),
   primary key (ID_USER, ID_MOVIE)
);

/*==============================================================*/
/* Table : MANAGE                                               */
/*==============================================================*/
create table MANAGE
(
   ID_USER              bigint not null,
   ID_MOVIE             bigint not null,
   SEEN                 bool,
   HAVE                 bool,
   RATE_OF_THE_USER     bigint,
   primary key (ID_USER, ID_MOVIE)
);

/*==============================================================*/
/* Table : MOVIE                                                */
/*==============================================================*/
create table MOVIE
(
   ID_MOVIE             bigint not null,
   TITLE                varchar(255),
   CONTENT              varchar(1024),
   RATE                 bigint,
   primary key (ID_MOVIE)
);

/*==============================================================*/
/* Table : USERS                                                */
/*==============================================================*/
create table USERS
(
   ID_USER              bigint not null,
   NAME                 varchar(255) not null,
   EMAIL                varchar(255) not null,
   EMAIL_VERIFIED       datetime,
   PASSWORD             varchar(255) not null,
   REMEMBER_TOKEN       varchar(100),
   CREATED_AT           datetime,
   UPDATED_AT           datetime,
   STATUT               bigint,
   primary key (ID_USER)
);

alter table BELONG add constraint FK_BELONG foreign key (ID_CATEGORY)
      references CATEGORY (ID_CATEGORY) on delete restrict on update restrict;

alter table BELONG add constraint FK_BELONG2 foreign key (ID_MOVIE)
      references MOVIE (ID_MOVIE) on delete restrict on update restrict;

alter table COMMENTS add constraint FK_COMMENTS foreign key (ID_USER)
      references USERS (ID_USER) on delete restrict on update restrict;

alter table COMMENTS add constraint FK_COMMENTS2 foreign key (ID_MOVIE)
      references MOVIE (ID_MOVIE) on delete restrict on update restrict;

alter table LEND add constraint FK_LEND foreign key (ID_USER)
      references USERS (ID_USER) on delete restrict on update restrict;

alter table LEND add constraint FK_LEND2 foreign key (ID_MOVIE)
      references MOVIE (ID_MOVIE) on delete restrict on update restrict;

alter table MANAGE add constraint FK_MANAGE foreign key (ID_USER)
      references USERS (ID_USER) on delete restrict on update restrict;

alter table MANAGE add constraint FK_MANAGE2 foreign key (ID_MOVIE)
      references MOVIE (ID_MOVIE) on delete restrict on update restrict;

